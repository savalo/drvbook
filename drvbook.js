/* Pondělí 15. března 2004 16:10 */

//////// promenne : ////////

var p = ['ID','state','editday','editdjz','editskm','editzkm','editkkm','editpredm','editmisto','editz','editzh','editzm','editd','editdh','editdm','editpmt','editl','editpkc','editokc','potvrd','smaz','uloz'];

//var tabulka = '<TR><TH>#<TH>Den<TH>Typ<TH>Start-Km<TH>Ujeto-Km<TH>Konec-Km<TH>Predmet<TH>Mesto jednani<TH>Vyjezd<TH>V-Hodina<TH>V-Minuta<TH>Prijezd<TH>P-Hodina<TH>P-Minuta<TH>Platba<TH>Litru<TH>Palivo<TH>Ostatni<TH>Potvrd<TH>Smaz<TH>Uloz<TH>Pridej';					// vse co se vypisuje

var backup = new Array(17);			// zalozni pole o kapacite jednoho radku
var posledni = 0;					// cislo posledniho zaznamu

//////// konec promennych ////////



////////////////////////////////// zacatek funkce "newline" //////////////////////////////////
function newline(indexradku)
{
	var tabulkajizd = document.getElementById("table");
	posledni = tabulkajizd.rows.length;

	if (!posledni) posledni = 8;

	if (!indexradku)		// podminka pro new
	{
		indexradku = 0;
		for (i=indexradku; i<posledni; i++)			// cyklus pro new
		{
			addline(i);
		}
	}
	else if (indexradku<posledni)	// pro pridani radku doprostred
	{

		//// zaloha radek od aktualniho az na konec
//		var rozdil = posledni-indexradku;
		var rowsbkp = [];		// pole o poctu odstranovanych radku
		

		for (i=indexradku; i<posledni; i++)
		{
			var linebkp = [];

			for (b=0;b<19;b++)
			{
				eval('linebkp.push(document.mainform.' + p[b] + (i+1) + '.value);');
			}

			rowsbkp.push(linebkp);		// ulozeni jednoho "row" do radku v poli
		}
		////

//alert(rowsbkp);

		//// smazani celych radek od aktualniho radku
		for (i=indexradku; i<posledni; i++)
		{
			tabulkajizd.deleteRow();
		}
		////

		//// vypsani radek od aktualniho az na konec + pridani radku
		for (i=indexradku; i<=posledni; i++)
		{
			addline(i);
			if (i==indexradku+1)
			{
				eval('document.mainform.ID' + i + '.value = 0');
			}
		}
		////

		//// doplneni dat ze zalohy
		for (i=indexradku+1,v=0; i<=posledni; i++,v++)
		{

			for (b=0; b<19; b++)
			{
				eval('document.mainform.' + p[b] + (i+1) + '.value =  rowsbkp[' + v + '][' + b + ']');
			}
		}
		////
	}
	else if (indexradku==posledni) addline(indexradku);		// pridani radku na konec

}
////////////////////////////////// konec funkce "newline" //////////////////////////////////



////////////////////////////////// zacatek funkce "delline" //////////////////////////////////
function delline(indexofline)
{
	var tabulkajizd = document.getElementById("table");
//	tabulkajizd.deleteRow(indexofline-1);
	for (d=1; d<23; d++)
	{
		tabulkajizd.rows(indexofline-1).deleteCell();
	}

	eval('document.mainform.state' + indexofline + '.value = "DLL"');

//	eval('document.mainform.uloz' + indexe + '.disabled = true');
//	document.mainform.ID
}
////////////////////////////////// konec funkce "delline" //////////////////////////////////



////////////////////////////////// zacatek funkce "addline" //////////////////////////////////
function addline(indexofline)
{
	var tabulkajizd = document.getElementById("table");		// deklarace tabulky
	tabulkajizd.insertRow(indexofline);						// vytvoreni noveho radku

	for (j=0; j<=p.length; j++)							// j<=p.length (23) je tam kvuli tlacitku ADD
	{
		tabulkajizd.rows(indexofline).insertCell(j);	// vytvareni novych bunek

		if (j==0 || j==1)		// skryta pole a prvni insert
		{
			tabulkajizd.rows(indexofline).cells(0).innerHTML = (indexofline + 1) + '<INPUT NAME="' + p[0] + (indexofline + 1) + '" TYPE=TEXT SIZE=1 VALUE="' + (indexofline + 1) + '"><INPUT NAME="' + p[1] + (indexofline + 1) + '" TYPE=TEXT SIZE=1 VALUE="FDB">';

		}
		else if (j==2)
		{
			tabulkajizd.rows(indexofline).cells(j-1).innerHTML = '<INPUT TYPE=TEXT NAME="' + p[2] + (indexofline + 1) + '" SIZE=1 VALUE="' + (indexofline + 1) + '" DISABLED>';
		}
		else if (j==3)
		{
			tabulkajizd.rows(indexofline).cells(j-1).innerHTML = '<SELECT NAME="' + p[j] + (indexofline + 1) + '" DISABLED><OPTION VALUE="0">--</OPTION><OPTION VALUE="1">on busi/obch</OPTION><OPTION VALUE="2">in priv/soukr</OPTION></SELECT>';
		}
		else if (j==15)
		{
			tabulkajizd.rows(indexofline).cells(j-1).innerHTML = '<SELECT NAME="' + p[j] + (indexofline + 1) + '" DISABLED><OPTION VALUE="0">--</OPTION><OPTION VALUE="1">Aral</OPTION><OPTION VALUE="2">&#214;MV</OPTION><OPTION VALUE="3">Shell</OPTION><OPTION VALUE="4">Cash Bus.</OPTION><OPTION VALUE="5">Cash Priv</OPTION></SELECT>';
		}
		else if (j==19)
		{
			tabulkajizd.rows(indexofline).cells(j-1).innerHTML = '<INPUT TYPE=BUTTON NAME="' + p[j] + (indexofline + 1) + '" VALUE="EDIT" onclick=editit(this.name,this.value)>';
		}
		else if (j==20)
		{
			tabulkajizd.rows(indexofline).cells(j-1).innerHTML = '<INPUT TYPE=BUTTON NAME="' + p[j] + (indexofline + 1) + '" VALUE="DELETE" DISABLED onclick=editit(this.name,this.value)>';
		}
		else if (j==21)
		{
			tabulkajizd.rows(indexofline).cells(j-1).innerHTML = '<INPUT TYPE=BUTTON NAME="' + p[j] + (indexofline + 1) + '" VALUE="SAVE" DISABLED onclick=editit(this.name,this.value)>';
		}
		else if (j==22)
		{
			tabulkajizd.rows(indexofline).cells(j-1).innerHTML = '<INPUT TYPE=BUTTON NAME="addline" VALUE="ADD" onclick=newline(' + (indexofline + 1) + ')>';
		}
		else
		{
			tabulkajizd.rows(indexofline).cells(j-1).innerHTML = '<INPUT TYPE=TEXT NAME="' + p[j] + (indexofline + 1) + '" SIZE=1 VALUE="' + (indexofline + 1) + '" DISABLED>';
		}
	}

}
////////////////////////////////// konec funkce "addline" //////////////////////////////////




////////////////////////////////// zacatek funkce "editit" //////////////////////////////////
function editit(jmeno,costim)
{

	switch(costim)
	{
		case "EDIT":
//alert(jmeno);
			indexe = jmeno.replace("potvrd","");	//vrati index radku na kterem bylo kliknuto
			eval('document.mainform.smaz' + indexe + '.disabled = false');
			eval('document.mainform.uloz' + indexe + '.disabled = false');
			eval('document.mainform.potvrd' + indexe + '.value = "CANCEL"');
			eval('document.mainform.state' + indexe + '.value = "EDT"');	// state
			for (l=2;l<19;l++)
			{
				eval('document.mainform.' + p[l] + indexe + '.disabled = false');
				eval('backup[' + l + '] = document.mainform.' + p[l] + indexe + '.value');
			}
			break;

		case "CANCEL":

			indexe = jmeno.replace("potvrd","");	//vrati index radku na kterem bylo kliknuto
			eval('document.mainform.smaz' + indexe + '.disabled = true');
			eval('document.mainform.uloz' + indexe + '.disabled = true');
			eval('document.mainform.potvrd' + indexe + '.value = "EDIT"');
			eval('document.mainform.state' + indexe + '.value = "FDB"');	// state
			for (l=2;l<19;l++)
			{
				eval('document.mainform.' + p[l] + indexe + '.disabled = true');
				eval('document.mainform.' + p[l] + indexe + '.value = backup[' + l + ']');
			}
			break;

		case "DELETE":

			indexe = jmeno.replace("smaz","");		//vrati index radku na kterem bylo kliknuto
			eval('document.mainform.state' + indexe + '.value = "DEL"');	// state
			for (l=2;l<19;l++)
			{
				if (l==3 || l==15)
				{
					eval('document.mainform.' + p[l] + indexe + '.value = "0"');
				}
				else
				{
					eval('document.mainform.' + p[l] + indexe + '.value = ""');
				}
			}
			break;

		case "SAVE":

			indexe = jmeno.replace("uloz","");		//vrati index radku na kterem bylo kliknuto
			eval('document.mainform.smaz' + indexe + '.disabled = true');
			eval('document.mainform.uloz' + indexe + '.disabled = true');
			eval('document.mainform.potvrd' + indexe + '.value = "EDIT"');
			for (l=2;l<19;l++)
			{
				eval('document.mainform.' + p[l] + indexe + '.disabled = true');
			}
			break;
	}

}
////////////////////////////////// konec funkce "editit" //////////////////////////////////



////////////////////////////////// zacatek funkce "validation" //////////////////////////////////

function validation()
{
	indexe = jmeno.replace("potvrd","");		//vrati index radku
	mesidz = "";

	for (indexe;indexe<posledni+1;indexe++)		// validation aktualniho radku
	{
		for (l=0;l<17;l++)
		{
			haur1 = "";
			haur2 = "";
			minit1 = "";
			minit2 = "";
			selekt1 = "";
			selekt2 = "";

			switch(l)	// switch of checkbox validation
			{
				case 1: if (eval('document.table.' + p[l] + indexe + '.value == "0"'))
						{
							selekt1 = "- Zvolte \"Typ jizdy\"    \n";
							eval('document.table.' + p[l] + indexe + '.focus()');
						}
						break;
				case 13: if (eval('document.table.' + p[l] + indexe + '.value == "0"'))
						{
							selekt2 = "- Zvolte \"Typ platby\"    \n";
							eval('document.table.' + p[l] + indexe + '.focus()');
						}
						break;
				case 8: if (eval('document.table.' + p[l] + indexe + '.value >= 24') || eval('document.table.' + p[l] + indexe + '.value < 0'))
						{
							haur1 = "- V policku \"Vyjezd hodina\" zadejte den ve formatu 0..23 hod  \n";
							eval('document.table.' + p[l] + indexe + '.focus()');
						}
						break;
				case 11: if (eval('document.table.' + p[l] + indexe + '.value >= 24') || eval('document.table.' + p[l] + indexe + '.value < 0'))
						{
							haur2 = "- V policku \"Prijezd hodina\" zadejte den ve formatu 0..23 hod  \n";
							eval('document.table.' + p[l] + indexe + '.focus()');
						}
						break;
				case 9: if (eval('document.table.' + p[l] + indexe + '.value >= 59') || eval('document.table.' + p[l] + indexe + '.value < 0'))
						{
							minit1 = "- V policku \"Vyjezd minuta\" zadejte hodinu ve formatu 0..59 min  \n";
							eval('document.table.' + p[l] + indexe + '.focus()');
						}
						break;
				case 12: if (eval('document.table.' + p[l] + indexe + '.value >= 59') || eval('document.table.' + p[l] + indexe + '.value < 0'))
						{
							minit2 = "- V policku \"Prijezd minuta\" zadejte hodinu ve formatu 0..59 min  \n";
							eval('document.table.' + p[l] + indexe + '.focus()');
						}
						break;
			}	// end of switch of checkbox validation


			if ((selekt1 != "") || (haur1 != "") || (minit1 != "") || (haur2 != "") || (minit2 != "") || (selekt2 != ""))
			{
				mesidz += "\nChyba na radku " + indexe + " : \n" + selekt1 + haur1 + minit1 + haur2 + minit2 + selekt2;
			}

		}	// konec cyklu na radek

	}	// konec cyklu na pocet radku

	if (mesidz!="") alert(mesidz);


// kontrola podle "posledni" - jestli je mensi zkontrolovat radky (indexe -> posledni)


// spocitani: aktualni radek (-> posledni radek) + vypsat do noveho

}

////////////////////////////////// konec funkce "validation" //////////////////////////////////
